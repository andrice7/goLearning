// Package singleton is a Singleton creational pattern that will give you the
// power to have a unique instance of some struct in your application and that
// no package can create any clone of this struct.
package singleton

// Singleton interface is the contract that verifies the unique instance of
// the singleton struct.
type Singleton interface {
	AddOne() int
}

// singleton is the structure of the single instance created from the package.
type singleton struct {
	count int
}

// This is the package level variable that will hold the instance and called in
// GetInstance()
var instance *singleton

// GetInstance is the function that will return the actual singleton instance.
// This function can be assigned to multiple variable and they will be assigned
// the single instance.
func GetInstance() Singleton {
	if instance == nil {
		instance = new(singleton)
	}
	return instance
}

// AddOne will add 1 everytime this function is called.
func (s *singleton) AddOne() int {
	s.count++
	return s.count
}
