// Conditions and Loops: Switch
package main

import (
	"fmt"
)

// Usage : go run example.8.go
func main() {
	i := 3
	switch i {
	case 1:
		fmt.Println("one")
	case 2:
		fmt.Println("two")
	case 3:
		fmt.Println("three")
	default:
		fmt.Println("something else")
	}
}
