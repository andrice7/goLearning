// Conditions and Loops: Switch
package main

import (
	"fmt"
)

// Usage : go run example.9.go
func main() {
	i := 2
	switch i {
	case 1, 2, 3:
		fmt.Println("one, two or three")
	default:
		fmt.Println("something else")
	}
}
