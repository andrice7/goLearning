// String
package main

import (
	"fmt"
)

// Usage : go run example.4.go
func main() {
	s := "hello, World!"
	r := []rune(s)  // change type
	r[0] = 'H'      // read new type
	s2 := string(r) // change type back
	fmt.Println(s2) // Print results which are the same
}
