// Variables & Constants
package main

import "fmt"

// Usage : go run example.1.go
func main() {
	var v1 int
	var v2 int
	v1 = 100
	fmt.Println("Value stored in variable v1 :: ", v1) // Value stored in variable v1 :: 100
	fmt.Println("Value stored in variable v2 :: ", v2) // Value stored in variable v2 :: 0
}
