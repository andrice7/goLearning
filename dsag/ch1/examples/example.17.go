// Function
package main

import (
	"fmt"
)

// Usage : go run example.17.go
func max(x, y int) int {
	if x > y {
		return x
	}
	return y
}
func main() {
	fmt.Println(max(10, 20))
	main1()
}

// IncrementPassByValue : This comment is necessary for exported functions
func IncrementPassByValue(x int) {
	x++
}
func main1() {
	i := 10
	fmt.Println("Value of i before increment is : ", i)
	IncrementPassByValue(i)
	fmt.Println("Value of i after increment is : ", i)
}
