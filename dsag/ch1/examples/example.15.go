// Conditions and Loops: For Loop
package main

import (
	"fmt"
)

// Usage : go run example.15.go
func main() {
	// Create slice of ints
	numbers := []int{1, 2, 3, 4, 5, 6, 7, 8, 9, 10}
	// Initialize sum variable
	sum := 0
	// Set up for loop using range in int slice
	for index, val := range numbers {
		// Add each value to sum variable
		sum += val
		fmt.Print("[", index, ",", val, "] ") // Print each array index and value
	}
	fmt.Println("\nSum is :: ", sum) // Print sum value
	// Create a map
	kvs := map[int]string{1: "apple", 2: "banana"}
	// Set up for loop using range of map
	for k, v := range kvs {
		fmt.Println(k, " -> ", v) // print key and values
	}
	str := "Hello, World!" // create a string
	// Set up for loop using range of string
	for index, c := range str {
		fmt.Print("[", index, ",", string(c), "] ") // print index and string value
	}
}
