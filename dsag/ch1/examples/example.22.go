// Structures
package main

import (
	"fmt"
)

// MyInt is a simple type
type MyInt int

func (data MyInt) increment1() {
	data = data + 1
}
func (data *MyInt) increment2() {
	*data = *data + 1 // The * is important. Has to be called to modify underlying data.
}
func main() {
	var data MyInt = 1
	fmt.Println("value before increment1() call :", data)
	data.increment1()
	fmt.Println("value after increment1() call :", data)
	data.increment2()
	fmt.Println("value after increment2() call :", data)
}
