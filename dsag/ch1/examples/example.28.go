// Array Interview Questions
package main

import (
	"fmt"
)

// SumArray method that will return the sum of all the elements of the integer list, given list as an input argument.
func SumArray(data []int) int {
	size := len(data)
	total := 0
	for index := 0; index < size; index++ {
		total = total + data[index]
	}
	return total
}

func main() {
	arr := []int{3, 4, 7, 11, 9, 2}
	sum := SumArray(arr)
	fmt.Println(sum)
}
