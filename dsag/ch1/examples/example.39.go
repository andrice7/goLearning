// Fibonacci number
package main

import "fmt"

// Permutation documentation
func Permutation(data []int, i int, length int) {
	if length == i {
		PrintSlice(data)
		return
	}
	for j := i; j < length; j++ {
		swap(data, i, j)
		Permutation(data, i+1, length)
		swap(data, i, j)
	}
}

func swap(data []int, x int, y int) {
	data[x], data[y] = data[y], data[x]
}

func main() {
	var data [5]int
	for i := 0; i < 5; i++ {
		data[i] = i
	}
	Permutation(data[:], 0, 5)
}

// PrintSlice just prints the slice
func PrintSlice(data []int) {
	fmt.Printf("%v :: len=%d cap=%d \n", data, len(data), cap(data))
}

// BinarySearchRecursive documentation
func BinarySearchRecursive(data []int, low int, high int, value int) int {
	mid := low + (high-low)/2 // To afunc the overflow
	if data[mid] == value {
		return mid
	} else if data[mid] < value {
		return BinarySearchRecursive(data, mid+1, high, value)
	} else {
		return BinarySearchRecursive(data, low, mid-1, value)
	}
}
