// Structures
package main

import (
	"fmt"
)

// Rect represents a rectangle. Rect has width and height fields
type Rect struct {
	width  float64
	height float64
}

// Area is a method for Rect that calculates Area.
func (r Rect) Area() float64 {
	return r.width * r.height
}

// Perimeter finds the length of all sides and adds them up.
func (r Rect) Perimeter() float64 {
	return 2 * (r.width + r.height)
}
func main() {
	r := Rect{width: 10, height: 10}
	fmt.Println("Area: ", r.Area())
	fmt.Println("Perimeter: ", r.Perimeter())
	fmt.Println(r)
	ptr := &Rect{width: 10, height: 5}
	fmt.Println(ptr)
	fmt.Println("Area: ", ptr.Area())
	fmt.Println("Perimeter: ", ptr.Perimeter())
}
