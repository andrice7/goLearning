// Fibonacci number
package main

import (
	"fmt"
)

func main() {
	fmt.Println("5 fibonacci is ", fibonacci(7))
}

// fibonacci documentation
func fibonacci(n int) int {
	if n <= 1 {
		return n
	}
	return fibonacci(n-1) + fibonacci(n-2)
}
