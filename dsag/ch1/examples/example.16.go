// Function
package main

import (
	"fmt"
)

// Usage : go run example.16.go
func max(x, y int) int {
	if x > y {
		return x
	}
	return y
}
func main() {
	fmt.Println(max(10, 20))
}
