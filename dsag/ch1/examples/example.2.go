// Variables & Constants
package main

import "fmt"

// Usage : go run example.2.go
func main() {
	v1 := 100
	fmt.Println("Value stored in variable v1 :: ", v1) // Value stored in variable v1 ::  100
}
