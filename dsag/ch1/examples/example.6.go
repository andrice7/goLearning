// Conditions and Loops
package main

import (
	"fmt"
)

// Usage : go run example.5.go
func main() {

	// which is max?
	fmt.Println("What is max of 3, 9? ", max(3, 9))

	fmt.Println("What is max of 24, 6? ", max(6, 24))
}

func max(x, y int) int {
	var max int
	if x > y {
		max = x
	} else {
		max = y
	}
	return max
}
