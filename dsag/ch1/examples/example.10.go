// Conditions and Loops: Switch
package main

import (
	"fmt"
)

// Usage : go run example.10.go
func main() {
	isEven(7)
	isEven(10)
	isEven(13)
}

func isEven(value int) {
	switch {
	case value%2 == 0:
		fmt.Println("I is even")
	default:
		fmt.Println("I is odd")
	}
}
