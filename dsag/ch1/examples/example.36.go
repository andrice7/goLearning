// Greatest Common Divisor
package main

import (
	"fmt"
)

func main() {
	fmt.Println(" Greatest Common Divisor of 4 and 6", GCD(12, 6))
}

// GCD documentation
func GCD(m int, n int) int {
	if m < n {
		return GCD(n, m)
	}
	if m%n == 0 {
		return n
	}
	return GCD(n, m%n)
}
