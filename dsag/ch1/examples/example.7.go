// Conditions and Loops: If with precondition.
package main

import (
	"fmt"
)

// Usage : go run example.5.go
func main() {

	// which is max?
	fmt.Println("The area of 5x by 5x should be less than 30x^2?", maxAreaCheck(5, 5, 30))
}

func maxAreaCheck(length, width, limit int) bool {
	if area := length * width; area < limit {
		return true
	}
	return false
}
