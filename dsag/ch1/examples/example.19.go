// Function
package main

import (
	"fmt"
)

// IncrementPassByPointer : go run example.19.go
func IncrementPassByPointer(ptr *int) {
	*ptr++
	(*ptr)++
}
func main() {
	i := 10
	fmt.Println("Value of i before increment is : ", i)
	IncrementPassByPointer(&i)
	fmt.Println("Value of i after increment is : ", i)
}
