// Conditions and Loops
package main

import (
	"fmt"
)

// Usage : go run example.5.go
func main() {
	min := 3
	max := 9

	// which is more?
	fmt.Println("Is 3 more than 9? ", more(min, max))

	fmt.Println("Is 9 more than 3? ", more(max, min))
}

func more(x, y int) bool {
	if x > y {
		return true
	}
	return false
}
