// Array Interview Questions
package main

import (
	"fmt"
)

// SequentialSearch method that will search all elements to find the value
func SequentialSearch(data []int, value int) bool {
	size := len(data)
	for i := 0; i < size; i++ {
		if value == data[i] {
			return true
		}
	}
	return false
}

func main() {
	arr := []int{3, 4, 7, 11, 9, 2}
	fmt.Println("Does ", arr, " have 9? ", SequentialSearch(arr, 9))
}
