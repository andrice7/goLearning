// Interface
package main

import (
	"fmt"
	"math"
)

// Shape interface has two methods that calculates the common functions of shapes
type Shape interface {
	Area() float64
	Perimeter() float64
}

// Rect is a representation of a rectangle. It has a height and width.
type Rect struct {
	width  float64
	height float64
}

// Circle is a representation of a circle and has a radius
type Circle struct {
	radius float64
}

// Area is a function that modifies Rect type
func (r Rect) Area() float64 {
	return r.width * r.height
}

// Perimeter finds the perimeter of a rectangle shape
func (r Rect) Perimeter() float64 {
	return 2 * (r.width + r.height)
}

// Area finds the area of a circle shape
func (c Circle) Area() float64 {
	return math.Pi * c.radius * c.radius
}

// Perimeter finds the perimeter of the circle type
func (c Circle) Perimeter() float64 {
	return 2 * math.Pi * c.radius
}

// TotalArea finds the areas of each shape and adds them all together.
func TotalArea(shapes ...Shape) float64 {
	var area float64
	for _, s := range shapes {
		area += s.Area()
	}
	return area
}

// TotalPerimeter finds the perimeter of each shape and adds them all together.
func TotalPerimeter(shapes ...Shape) float64 {
	var peri float64
	for _, s := range shapes {
		peri += s.Perimeter()
	}
	return peri
}

func main() {
	r := Rect{width: 10, height: 10}
	c := Circle{radius: 10}
	fmt.Println("Total Area: ", TotalArea(r, c))
	fmt.Println("Total Perimeter: ", TotalPerimeter(r, c))
}
