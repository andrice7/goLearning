// Array Interview Questions
package main

import (
	"fmt"
)

// RotateArray will move by a certain position
func RotateArray(data []int, k int) {
	n := len(data)
	ReverseArray(data, 0, k-1)
	ReverseArray(data, k, n-1)
	ReverseArray(data, 0, n-1)
}

// ReverseArray will switch array?
func ReverseArray(data []int, start int, end int) {
	i := start
	j := end
	for i < j {
		data[i], data[j] = data[j], data[i]
		i++
		j--
	}
}

func main() {
	arr := []int{10, 20, 30, 40, 50, 60}
	fmt.Println(arr)
	RotateArray(arr, 2)
	fmt.Println(arr)
}
