/*
Chapter 1 - Exercise 12
Given an array of 0s, 1s and 2s. We need to sort it so that all the 0s are
before all the 1s and all the 1s are before 2s.
Hint: Same as above first think 0s and 1s as one group and move all the 2s
on the right side. Then do a second pass over the array to sort 0s and 1s.
*/
package main

import "fmt"

// Permutation documentation
func Permutation(data []int, i int, length int) {
	if length == i {
		PrintSlice(data)
		return
	}
	for j := i; j < length; j++ {
		swap(data, i, j)
		Permutation(data, i+1, length)
		swap(data, i, j)
	}
}

func swap(data []int, x int, y int) {
	data[x], data[y] = data[y], data[x]
}

func main() {
	data := []int{2, 3, 3}
	Permutation(data[:], 0, len(data))
}

// PrintSlice just prints the slice
func PrintSlice(data []int) {
	fmt.Printf("%v :: len=%d cap=%d \n", data, len(data), cap(data))
}
