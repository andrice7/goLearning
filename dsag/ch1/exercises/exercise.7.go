/*
Chapter 1 - Exercise 7
Write a method to compute Sum(N) = 1+2+3+...+N.
*/
package main

import "fmt"

// Sum documentation
func Sum(num int) int {
	var sum int
	for i := 0; i <= num; i++ {
		sum += i
	}
	return sum
}

func main() {
	fmt.Println(Sum(20))
}
