/*
Chapter 1 - Exercise 10
Reverse a list in-place. (You cannot use any additional list in other wards
Space Complexity should be O(1). )
Hint: Use two variable, start and end. Start set to 0 and end set to (n-1).
Increment start and decrement end. Swap the values stored at arr[start] and
arr[end]. Stop when start is equal to end or start is greater than end.
*/
package main

import "fmt"

// Permutation documentation
func Permutation(data []int, i int, length int) {
	if length == i {
		PrintSlice(data)
		return
	}
	for j := i; j < length; j++ {
		swap(data, i, j)
		Permutation(data, i+1, length)
		swap(data, i, j)
	}
}

func swap(data []int, x int, y int) {
	data[x], data[y] = data[y], data[x]
}

func main() {
	data := []int{2, 3, 3}
	Permutation(data[:], 0, len(data))
}

// PrintSlice just prints the slice
func PrintSlice(data []int) {
	fmt.Printf("%v :: len=%d cap=%d \n", data, len(data), cap(data))
}
