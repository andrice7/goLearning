/*
Chapter 1 - Exercise 13
Find the duplicate elements in a list of size n where each element is in the
range 0 to n-1.
Hint:
Approach 1: Compare each element with all the elements of the list (using
two loops) O(n 2 ) solution
Approach 2: Maintain a Hash-Table. Set the hash value to 1 if we encounter
the element for the first time. When we same value again we can see that the
hash value is already 1 so we can print that value. O(n) solution, but
additional space is required.
Approach 3: We will exploit the constraint "every element is in the range 0
to n-1". We can take a list arr[] of size n and set all the elements to 0.
Whenever we get a value say val1. We will increment the value at arr[var1]
index by 1. In the end, we can traverse the list arr and print the repeated
values. Additional Space Complexity will be O(n) which will be less than
Hash-Table approach.
*/
package main

import "fmt"

// Permutation documentation
func Permutation(data []int, i int, length int) {
	if length == i {
		PrintSlice(data)
		return
	}
	for j := i; j < length; j++ {
		swap(data, i, j)
		Permutation(data, i+1, length)
		swap(data, i, j)
	}
}

func swap(data []int, x int, y int) {
	data[x], data[y] = data[y], data[x]
}

func main() {
	data := []int{2, 3, 3}
	Permutation(data[:], 0, len(data))
}

// PrintSlice just prints the slice
func PrintSlice(data []int) {
	fmt.Printf("%v :: len=%d cap=%d \n", data, len(data), cap(data))
}
