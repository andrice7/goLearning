// Chapter 1 - Exercise 3
// Find the largest element in the list.
package main

import "fmt"

func main() {
	// create a large list
	data := []int{4, 8, 2, 2, 9, 34, 32, 9, 10, 3, 22, 18, 6, 0}
	BubbleSort(data, more)         // Sort from small to big
	fmt.Println(data)              // Check data manually
	fmt.Println(data[len(data)-1]) // Print largest number which is last in list.

}

// BubbleSort : Sort the list in ascending order
func BubbleSort(arr []int, comp func(int, int) bool) {
	size := len(arr)
	for i := 0; i < (size - 1); i++ {
		for j := 0; j < (size - i - 1); j++ {
			if comp(arr[j], arr[j+1]) {
				arr[j+1], arr[j] = arr[j], arr[j+1]
			}
		}
	}
}

// more : find the largest number
func more(value1, value2 int) bool {
	return value1 > value2
}
