/*
Chapter 1 - Exercise 8
Print all the maxima’s in a list. (A value is a maximum if the value before
and after its index are smaller than it is or does not exist.)
Hint: Start traversing list from the end and keep track of the max element.
If we encounter an element whose value is grater then max, print the element
and update max.
*/
package main

import "fmt"

// AllMaximas prints all the maxima's in a list.
func AllMaximas(data []int) {
	size := len(data)
	var max int
	for i := size - 1; i >= 0; i-- {
		// Check left side
		if more(data[i], data[i-1]) && less(data[i+1],data[i])
	}
}

func main() {
	data := []int{1, 3, 4, 5, 6, 6, 5, 3, 2, 1, 2, 3, 4, 5, 4, 3, 2, 1, 5, 3, 2, 3}
	AllMaximas(data)
}

func more(value1 int, value2 int) bool {
	return value1 > value2
}

func equal(value1 int, value2 int) bool {
	return value1 == value2
}

func less(value1, value2 int) bool {
	return value1 < value2
}