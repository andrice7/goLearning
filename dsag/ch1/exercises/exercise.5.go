// Chapter 1 - Exercise 5
// Find the second largest number in the list.
package main

import "fmt"

func main() {
	// create list
	data := []int{4, 8, 2, 2, 9, 34, 32, 9, 10, 3, 22, 18, 6, 0}
	BubbleSort(data, more)
	fmt.Println(data)
	fmt.Println(data[len(data)-2]) // Find second largest

}

// BubbleSort : The slowest type of sorting algorithm
func BubbleSort(arr []int, comp func(int, int) bool) {
	size := len(arr) // Always usefull to get info from data set
	for i := 0; i < (size - 1); i++ {
		for j := 0; j < (size - i - 1); j++ {
			if comp(arr[j], arr[j+1]) {
				// Swapping
				arr[j+1], arr[j] = arr[j], arr[j+1]
			}
		}
	}
}

func more(value1 int, value2 int) bool {
	return value1 > value2
}

func less(value1, value2 int) bool {
	return value1 < value2
}
