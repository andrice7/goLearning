/*
Chapter 1 - Exercise 14
Find the maximum element in a sorted and rotated list.
Complexity: O(logn)
Hint: Use binary search algorithm.
*/
package main

import "fmt"

// Permutation documentation
func Permutation(data []int, i int, length int) {
	if length == i {
		PrintSlice(data)
		return
	}
	for j := i; j < length; j++ {
		swap(data, i, j)
		Permutation(data, i+1, length)
		swap(data, i, j)
	}
}

func swap(data []int, x int, y int) {
	data[x], data[y] = data[y], data[x]
}

func main() {
	data := []int{2, 3, 3}
	Permutation(data[:], 0, len(data))
}

// PrintSlice just prints the slice
func PrintSlice(data []int) {
	fmt.Printf("%v :: len=%d cap=%d \n", data, len(data), cap(data))
}
