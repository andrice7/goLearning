/*
Chapter 1 - Exercise 9
Given a list of intervals, merge all overlapping intervals.
Input: {[1, 4], [3, 6], [8, 10]},
Output: {[1, 6], [8, 10]}
*/
package main

import "fmt"

// Permutation documentation
func Permutation(data []int, i int, length int) {
	if length == i {
		PrintSlice(data)
		return
	}
	for j := i; j < length; j++ {
		swap(data, i, j)
		Permutation(data, i+1, length)
		swap(data, i, j)
	}
}

func swap(data []int, x int, y int) {
	data[x], data[y] = data[y], data[x]
}

func main() {
	data := []int{2, 3, 3}
	Permutation(data[:], 0, len(data))
}

// PrintSlice just prints the slice
func PrintSlice(data []int) {
	fmt.Printf("%v :: len=%d cap=%d \n", data, len(data), cap(data))
}
