// Chapter 1 - Exercise 2
// Find the sum of all the elements of a two dimensional list.
package main

import "fmt"

func main() {
	// create a two dimensional list
	ints := [][]int{
		{4, 8, 2, 2, 9, 34, 32, 12},
		{9, 10, 3, 22, 18, 6, 0},
	}

	total := 0

	for _, data := range ints {
		total = total + Sum(data)
	}
	// print the results
	fmt.Println(total)
}

// Sum : Sum function returns the sum of a two dimensional
// slice of integers
func Sum(data []int) int {
	size := len(data) // size of slice
	total := 0        // initialized variable
	for index := 0; index < size; index++ {
		total = total + data[index] // add to variable
	}
	return total // Return variable
}
