// Chapter 1 - Exercise 1
// Find average of all the elements in a list.
package main

import "fmt"

func main() {
	// create slice of ints
	ints := []int{4, 8, 2, 2, 9, 34, 32, 12}
	// print the results
	fmt.Println(avg(ints))
}

// avg: Average function returns the average given
// given a set of integers
func avg(data []int) int {
	size := len(data) // size of slice
	total := 0        // initialized variable
	for index := 0; index < size; index++ {
		total = total + data[index] // add to variable
	}
	return total / size // Return variable
}
