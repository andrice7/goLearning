package main

import (
	"fmt"
)

func main() {
	data := []int{1, 2, 3, 3}
	DistinctPermutations(data)
}

// DistinctPermutations prints distict permutations of an int list
func DistinctPermutations(data []int) {
	// Get the size of the array
	size := len(data)

	// Sort in ascending order
	BubbleSort(data, more)

	isFinished := false
	// For loop while isFinished is false
	for !isFinished {
		// Print the permutation
		PrintSlice(data)

		// Loop through to find right most character
		var i int
		i = size - 2
		for ; i >= 0; i-- {
			if data[i] < data[i+1] {
				break
			}
		}
		fmt.Println(i)
		if i == -1 {
			isFinished = true
		} else {
			ceilIndex := findCeil(data, data[i], i+1, size-1)
			fmt.Println(ceilIndex)
			swap(data, i, ceilIndex)

			BubbleSort(data[i+1:size], more)
		}
	}
}

// findCeil is used to
func findCeil(data []int, first, l, h int) int {
	// initialize index of ceiling element
	ceilIndex := l

	// Now iterate through rest of elements and find
	// the smallest character greater than 'first'
	for i := l + 1; i <= h; i++ {
		if data[i] > first && data[i] < data[ceilIndex] {
			ceilIndex = i
		}
	}

	return ceilIndex
}

// BubbleSort : The slowest type of sorting algorithm
func BubbleSort(arr []int, comp func(int, int) bool) {
	size := len(arr) // Always usefull to get info from data set
	for i := 0; i < (size - 1); i++ {
		for j := 0; j < (size - i - 1); j++ {
			if comp(arr[j], arr[j+1]) {
				// Swapping
				arr[j+1], arr[j] = arr[j], arr[j+1]
			}
		}
	}
}

func more(value1 int, value2 int) bool {
	return value1 > value2
}

func less(value1, value2 int) bool {
	return value1 < value2
}

func swap(data []int, x int, y int) {
	if x == y {
		return
	}
	data[x], data[y] = data[y], data[x]
}

// PrintSlice just prints the slice
func PrintSlice(data []int) {
	fmt.Printf("%v :: len=%d cap=%d \n", data, len(data), cap(data))
}
