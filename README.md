# Golang Learning Library

GoLearning is a collection of projects and exercises completed from a collection of books and online resources.

## Data Structures & Algorithms in Go

/dsag contains the exercises and projects for this book. 

## Go Programming Language

/gopl contains the exercises and projects for this book.