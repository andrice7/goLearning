// Package abstract_factory file: car.go
package abstract_factory

type Car interface {
	NumDoors() int
}
